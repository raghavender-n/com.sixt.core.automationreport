package com.automation.report.controller;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.testng.IAnnotationTransformer;
import org.testng.annotations.ITestAnnotation;

/**
 * @author rishiahluwalia
 * 
 *         Annotation Transformer is used to modify the content of all the
 *         annotations at run time. This is useful when we want to override the
 *         annotation values.
 * 
 *         IAnnotationTransformer” is an interface and it’s one of the TestNG
 *         listeners. It has a method “transform”.
 * 
 *         IAnnotationTransformer allows you to modify a @Test annotation.
 * 
 */
public class AnnotationTransformer implements IAnnotationTransformer {

	public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {

		// This will set retryAnalyzer to the test methods if not set in class level
		if (annotation.getRetryAnalyzer() == null) {
			annotation.setRetryAnalyzer(TestRetryAnalyzer.class);
		}

	}

}