package com.automation.report.controller;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Appender;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.testng.Reporter;


/**
 * Provides log methods for Web and API testing
 * 
 */
public class Logging {

	private static Map<String, Map<String, Map<String, List<String>>>> pageListenerLogMap = Collections
			.synchronizedMap(new HashMap<String, Map<String, Map<String, List<String>>>>());

	
	/**
	 * Log method
	 * 
	 * @param message
	 */
	public static void error(String message) {
		message = "<li><b><font color='#FF0000'>" + message
				+ "</font></b></li>";
		log(null, message, false, false);
	}

	public static Logger getLogger(Class<?> clz) {
		boolean rootIsConfigured = Logger.getRootLogger().getAllAppenders()
				.hasMoreElements();
		if (!rootIsConfigured) {
			BasicConfigurator.configure();
			Logger.getRootLogger().setLevel(Level.INFO);
			Appender appender = (Appender) Logger.getRootLogger()
					.getAllAppenders().nextElement();
			appender.setLayout(new PatternLayout(" %-5p %d [%t] %C{1}: %m%n"));
		}
		return Logger.getLogger(clz);
	}

	public static Map<String, Map<String, List<String>>> getPageListenerLog(
			String pageListenerClassName) {
		return pageListenerLogMap.get(pageListenerClassName);
	}

	
	/**
	 * Log method
	 * 
	 * @param message
	 */
	public static void info(String message) {
		message = "<li><font color='#008000'>" + message + "</font></li>";
		log(null, message, false, false);
	}

	/**
	 * Log method
	 * 
	 * @param message
	 */
	public static void log(String message) {
		log(null, message, false, false);
	}

	/**
	 * Log
	 * 
	 * @param message
	 * @param logToStandardOutput
	 */
	public static void log(String message, boolean logToStandardOutput) {
		log(null, message, false, logToStandardOutput);
	}

	public static void log(String url, String message, boolean failed,
			boolean logToStandardOutput) {

		if (message == null)
			message = "";

		message = message.replaceAll("\\n", "<br/>");

		if (failed) {
			message = "<span style=\"font-weight:bold;color:#FF0066;\">"
					+ message + "</span>";
		}

		Reporter.log(message, logToStandardOutput); 
		//Reporter.log(escape(message), logToStandardOutput); // fix for testng6.7
	}

	public static String escape(String message) {
		return message.replaceAll("\\n", "<br/>").replaceAll("<", "@@lt@@")
				.replaceAll(">", "^^gt^^");
	}

	
	public static String getDate() {
		SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss,SSS ");
		return df.format(new Date());
	}

	public static void logAPICall(String url, String message, boolean failed) {
		log(url, "<li>"
				+ (failed ? getDate() + "<b>FailedAPICall</b>: " : getDate()
						+ "APICall: ") + message + "</li>", failed,
				false);
	}

	public static void logDBCall(String message, boolean failed) {
		log(null, "<li>"
				+ (failed ? getDate() + "<b>FailedDBCall</b>: " : getDate()
						+ "DBCall: ") + message + "</li>", failed, false);
	}

	public static void logEmailStep(String message, boolean failed) {
		log(null, "<li>"
				+ (failed ? getDate() + "<b>FailedEmailStep</b>: " : getDate()
						+ "EmailStep: ") + message + "</li>", failed, false);
	}

	public static void logRESTAPICall(String url, String message, boolean failed) {
		log(url, "<li>"
				+ (failed ? getDate() + "<b>FailedRESTAPICall</b>: " : getDate()
						+ "RESTAPICall: ") + message + "</li>", failed,
				false);
	}
	
	public static void logSSHCall(String message, boolean failed) {
		log(null, "<li>"
				+ (failed ? getDate() + "<b>FailedSSHStep</b>: " : getDate()
						+ "SSHStep: ") + message + "</li>", failed, false);
	}

	public static void logWebOutput(String url, String message, boolean failed) {
		log(url, getDate() + "Output: " + message + "<br/>", failed, false);
	}

	public static void logWebStep(String url, String message, boolean failed) {
		log(url, "<li>"
				+ (failed ? getDate() + "<b>FailedStep</b>: " : getDate()
						+ "Step: ") + message + "</li>", failed, false);
	}

	
	/**
	 * Log method
	 * 
	 * @param message
	 */
	public static void warning(String message) {
		message = "<li><font color='#FFFF00'>" + message + "</font></li>";
		log(null, message, false, false);
	}
	
}
