package com.automation.report.controller;

import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestResult;

import java.io.File;
import java.util.*;
import java.util.Map.Entry;

public class Context {

	public static final String BROWSER = "browser";
	public static final String HOSTADDRESS = "host";
	public static final String TEST_RETRY_COUNT = "testRetryCount";
	public static final String TEST_RETRY_INTERVAL = "testRetryInterval";
	public static final String OPEN_REPORT_IN_BROWSER = "openReportInBrowser";
	public static final String HEALTH_CHECK = "healthCheck";
	public static final String ENVIRONMENT = "environment_type";
	public static final String TEST_CONFIG = "testConfig";
	public static final String TEST_START_TIME = "time";

	private static Logger logger = Logging.getLogger(Context.class);
	private Map<ITestResult, List<Throwable>> verificationFailuresMap = new HashMap<ITestResult, List<Throwable>>();
	private Map<String, Object> contextDataMap = Collections.synchronizedMap(new HashMap<String, Object>());
	private ITestContext testNGContext = null;

	public Context(ITestContext context) {
		this.testNGContext = context;
		this.setContextAttribute(context, "browser", System.getProperty("browser"), "*Firefox");
		this.setContextAttribute(context, "host", System.getProperty("host"),
				"https://sixt--dci--c.cs85.visual.force.com/apex/drmapp#/");
		this.setContextAttribute(context, "testRetryInterval", System.getProperty("testRetryInterval"), "5");
		this.setContextAttribute(context, "testRetryCount", System.getProperty("testRetryCount"), "3");
		this.setContextAttribute(context, "time", System.currentTimeMillis()+"", System.currentTimeMillis()+"");
		this.setContextAttribute(context, "environment_type", System.getProperty("environment_type"), "TEST");
		this.setContextAttribute(context, "healthCheck", System.getProperty("healthCheck"), "false");
		this.setContextAttribute(context, "test_case_execution", System.getProperty("test_case_execution"), "true");
		this.setContextAttribute((ITestContext) context, "openReportInBrowser",
				System.getProperty("openReportInBrowser"), (String) null);
		if (context != null) {
			this.setContextAttribute((String) "outputDirectory", (String) null, context.getOutputDirectory(),
					(String) null);
			this.setContextAttribute(context);
			(new File(context.getOutputDirectory() + "/screenshots")).mkdirs();
			(new File(context.getOutputDirectory() + "/htmls")).mkdirs();
			(new File(context.getOutputDirectory() + "/xmls")).mkdirs();
			(new File(context.getOutputDirectory() + "/textfiles/")).mkdirs();
		}

	}

	public void addVerificationFailures(ITestResult result, List<Throwable> failures) {
		this.verificationFailuresMap.put(result, failures);
	}

	public void addVerificationFailures(ITestResult result, Throwable failure) {
		if (this.verificationFailuresMap.get(result) != null) {
			((List) this.verificationFailuresMap.get(result)).add(failure);
		} else {
			ArrayList failures = new ArrayList();
			failures.add(failure);
			this.addVerificationFailures(result, (List) failures);
		}

	}

	public Object getAttribute(String name) {
		Object obj = this.contextDataMap.get(name);
		return obj == null ? "Firefox" : obj;
	}

	public String getBrowserDownloadDir() {
		return this.getAttribute("browserDownloadDir") != null ? (String) this.getAttribute("browserDownloadDir")
				: this.getOutputDirectory() + "\\downloads\\";
	}

	public double getEmailRetentionPolicy() {
		try {
			return Double.parseDouble((String) this.getAttribute("emailRetentionPolicy"));
		} catch (Exception var2) {
			return 1.0D;
		}
	}

	public String getOpenReportInBrowser() {
		return (String) this.getAttribute("openReportInBrowser");
	}

	public void setOpenReportInBrowser(String browser) {
		this.setAttribute("openReportInBrowser", browser);
	}

	public String getOutputDirectory() {
		return (String) this.getAttribute("outputDirectory");
	}

	public String getHostAddress() {
		return (String) this.getAttribute("host");
	}

	public String getTestRetryCount() {
		return (String) this.getAttribute("testRetryCount");
	}

	public String getTestRetryInterval() {
		return (String) this.getAttribute("testRetryInterval");
	}

	public String getTime() {
		return (String) this.getAttribute("time");
	}

	public String getEnvironment() {
		return (String) this.getAttribute("environment_type");
	}

	public String getSuiteParameter(String key) {
		return System.getProperty(key) != null ? System.getProperty(key)
				: this.getTestNGContext().getSuite().getParameter(key);
	}

	public String getTestMethodSignature() {
		return (String) this.getAttribute("testMethodSignature");
	}

	public ITestContext getTestNGContext() {
		return this.testNGContext;
	}

	public List<Throwable> getVerificationFailures(ITestResult result) {
		List verificationFailures = (List) this.verificationFailuresMap.get(result);
		return (List) (verificationFailures == null ? new ArrayList() : verificationFailures);
	}

	public int getWebSessionTimeout() {
		try {
			return Integer.parseInt((String) this.getAttribute("webSessionTimeOut"));
		} catch (Exception var2) {
			return 90000;
		}
	}

	public void setAttribute(String name, Object value) {
		this.contextDataMap.put(name, value);
	}

	private void setContextAttribute(ITestContext context) {
		if (context != null) {
			Map testParameters = context.getSuite().getXmlSuite().getParameters();
			Iterator var3 = testParameters.entrySet().iterator();

			while (var3.hasNext()) {
				Entry entry = (Entry) var3.next();
				String attributeName = (String) entry.getKey();
				if (this.contextDataMap.get(entry.getKey()) == null) {
					String sysPropertyValue = System.getProperty((String) entry.getKey());
					String suiteValue = (String) entry.getValue();
					this.setContextAttribute((String) attributeName, sysPropertyValue, suiteValue, (String) null);
				}
			}
		}

	}

	private void setContextAttribute(ITestContext context, String attributeName, String sysPropertyValue,
			String defaultValue) {
		String suiteValue = null;
		if (context != null) {
			suiteValue = context.getSuite().getParameter(attributeName);
		}

		this.contextDataMap.put(attributeName,
				sysPropertyValue != null ? sysPropertyValue : (suiteValue != null ? suiteValue : defaultValue));
	}

	public String getContextAttribute(String attributeName, String defaultValue) {
		String suiteValue = null;
		if (testNGContext != null) {
			suiteValue = testNGContext.getSuite().getParameter(attributeName);
		}
		suiteValue = suiteValue != null ? suiteValue : defaultValue;
		this.contextDataMap.put(attributeName, suiteValue);
		return suiteValue;

	}

	private void setContextAttribute(String attributeName, String sysPropertyValue, String suiteValue,
			String defaultValue) {
		this.contextDataMap.put(attributeName,
				sysPropertyValue != null ? sysPropertyValue : (suiteValue != null ? suiteValue : defaultValue));
	}

	public void setHOSTADDRESS(String HOSTADDRESS) {
		this.setAttribute(HOSTADDRESS, HOSTADDRESS);
	}

	public String getHealthCheck() {
		return (String) this.getAttribute("healthCheck");
	}

}
