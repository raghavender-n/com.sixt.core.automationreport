package com.automation.report.controller;

import java.util.HashMap;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

/**
 * Implement to be able to have a chance to retry a failed test to solve below
 * use cases 
 * 1) Random browser issues or browser becoming unresponsive 
 * 2) Random machine issues 
 * 3) Server issues like unexpected delay in the response from server
 *
 */
public class TestRetryAnalyzer implements IRetryAnalyzer {

	private static final String TEST_RETRY_COUNT = "testRetryCount";
	private static final String TEST_RETRY_INTERVAL = "testRetryInterval";
	private int count = 1;
	private int maxCount = 3;
	public static HashMap<String, Integer> retryTestsDetails = new HashMap<String, Integer>();

	public TestRetryAnalyzer() {
		String retryMaxCount = System.getProperty(TEST_RETRY_COUNT);
		if (retryMaxCount != null) {
			maxCount = Integer.parseInt(retryMaxCount);
		}
	}

	public void setMaxCount(int count) {
		this.maxCount = count;
	}

	public int getCount() {
		return this.count;
	}

	public int getMaxCount() {
		return this.maxCount;
	}

	/**
	 * Returns true if the test method has to be retried, false otherwise.
	 *
	 * @param result The result of the test method that just ran.
	 * @return true if the test method has to be retried, false otherwise.
	 */
	public synchronized boolean retry(ITestResult result) {
		String testClassName = String.format("%s.%s", result.getMethod().getRealClass().toString(),
				result.getMethod().getMethodName());

		if (count <= maxCount) {
			result.setAttribute("RETRY", count);

			Logging.log("[RETRYING] " + testClassName + " FAILED, " + "Retrying " + count + " time", true);
			count += 1;

			String retryInterval = System.getProperty(TEST_RETRY_INTERVAL);
			if (retryInterval != null) {
				try {
					int intervalSeconds = Integer.parseInt(retryInterval);
					Logging.log("Retry after " + intervalSeconds + " seconds", true);
					Thread.sleep(intervalSeconds * 1000);
				} catch (Exception ex) {
				}
			}

			if (retryTestsDetails.get(result.getClass().getName() + result.getMethod()) != null) {
				retryTestsDetails.put(result.getClass().getName() + result.getMethod(),
						retryTestsDetails.get(result.getClass().getName() + result.getMethod()) + 1);
			} else {
				retryTestsDetails.put(result.getClass().getName() + result.getMethod(), 1);
			}

			return true;
		}
		return false;
	}
}
